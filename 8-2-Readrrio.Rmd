# Reading data with packages: `rio`

```{r echo = TRUE}
if (!requireNamespace("rio", quietly = TRUE)) { # if not yet installed,
  install.packages("rio")                       # install package rio
}
```


```{r echo=TRUE}
library(rio)
ds1 <- import("ExerciseCoercion.xlsx")
DT::datatable(ds1)
```

# Writing data with packages: `rio`

```{r echo=TRUE}
ds1$date[[3]] <- NA
ds1$date <- as.integer(ds1$date)
if (!requireNamespace("openxlsx", quietly = TRUE)) { # if not yet installed,
  install.packages("openxlsx")                       # install package openxlsx
}
library(openxlsx)
ds1$date <- convertToDate(ds1$date) # this takes care for Excel spcification errors
```


```{r echo=TRUE, eval = FALSE}
library(rio)
export(ds1, "/tmp/test.ods")
if (interactive()) {
  if (!requireNamespace("berryFunctions", quietly = TRUE)) {
    install.packages("berryFunctions")
  }
  berryFunctions::openFile("ExerciseCoercion.xlsx")
  berryFunctions::openFile("/tmp/test.ods")
}
```


# Reading data with packages: `readr` 

- CSV with fine-grained control

```{r echo = TRUE}
if (!requireNamespace("readr", quietly = TRUE)) { # if not yet installed,
  install.packages("readr")                       # install package readr
}
```

```{r echo=TRUE, warning=TRUE}
library(readr)
# source: https://people.sc.fsu.edu/~jburkardt/data/csv/csv.html
hurricanes <- 
  read_csv("https://people.sc.fsu.edu/~jburkardt/data/csv/hurricanes.csv")
DT::datatable(hurricanes)
```

# Reading data with packages: `readr` with column specification I

```{r echo=TRUE, warning=TRUE}
print(spec(hurricanes))
hurricanes <- 
  read_csv("https://people.sc.fsu.edu/~jburkardt/data/csv/hurricanes.csv", 
           col_types = cols(
              Month = col_character(),
              Average = col_double(),
              `2005` = col_integer(),
              `2006` = col_integer(),
              `2007` = col_integer(),
              `2008` = col_integer(),
              `2009` = col_integer(),
              `2010` = col_integer(),
              `2011` = col_integer(),
              `2012` = col_integer(),
              `2013` = col_integer(),
              `2014` = col_integer(),
              `2015` = col_integer()
           ))
```

# Reading data with packages: `readr` with column specification II

```{r echo=TRUE, warning=TRUE}
DT::datatable(hurricanes)
print(spec(hurricanes))
```

