# Common statistical R object types

- vectors $\mathbb{R}^1$

```{r}
v2 <- 1:5
v2
v3 <- letters[1:5]
v3
```


- matrices $\mathbb{R}^2$

  - $\rightarrow$ filled by column (default)!

```{r}
m1 <- matrix(1:9, ncol = 3)
m1
```

-
  - $\rightarrow$ filled by row

```{r}
m2 <- matrix(1:9, ncol = 3, byrow = TRUE)
m2
```

- arrays $\mathbb{R}^n$

```{r}
a1 <- array(NA, dim = c(3, 3, 2))
a1

a1[ , , 1] <- m1
a1[ , , 2] <- m2
a1
```

- data.frame $\mathbb{R}^2$, columns' data types may differ

```{r}
data.frame(Numbers = v2, Letters = v3)
```

- lists: any combination of heterogeneous R objects

```{r}
list(Numbers = v2, Letters = v3, Presidents = 
       c("Washington, G",
         "Lincoln, A",
         "Bush, Sr.",
         "Roosevelt, F",
         "Trueman, H",
         "Reagan, R",
         "Trump, D",
         "Biden, J"))
```


