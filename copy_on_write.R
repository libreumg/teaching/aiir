tracemem(a)
tracemem(b)

a <- 42
1234

b <- a
1234

(1:4)[2]

b <- 99
1234

1235

a <- c()
length(a)

a <- c(NA, NA)
length(a)


name <- "Stephan"

a <- 4 + 5

print(a + 5)

force(a)

a <- 42

a + 1

# 1000 lines of code

paste(a, "y")

fu <- function() {
  a <- 99
}

fu2 <- function() {
  a <- 9992
}

untracemem(a)
untracemem(b)


x <- LETTERS[3:5]

f <- as.factor(x)

TRUE & FALSE

c(TRUE, TRUE, TRUE) & c(FALSE, TRUE, TRUE, 
                        FALSE, TRUE, TRUE)

3 * 1:4
c(3, 3, 3, 3) * c(1, 2, 3, 4)

c(TRUE, TRUE, TRUE) && c(FALSE, TRUE, TRUE, 
                         FALSE, TRUE, TRUE)

if (a == 42 && print(b == 19)) {
  print("Hi")
}
