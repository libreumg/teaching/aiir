---
title: "TIIR - Technical Introduction Into R"
author: "Stephan Struckmann"
date: "January 2022"
output: slidy_presentation
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

# Literature

- Hadley Wickham: Advanced R, 2nd edition, https://adv-r.hadley.nz/
- Our AIIR course: https://gitlab.com/libreumg/teaching/aiir

# Figures

Most of the illustrations have been downloaded from 
[https://github.com/allisonhorst/stats-illustrations](https://github.com/allisonhorst/stats-illustrations){target="_blank"}
licensed under the [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/){target="_blank"}.
No modifications have been applied to these images.

Some figures are Unicode-Characters.

Some figures have been drawn by Stephan Struckmann.

# Preface: Some background, definitions, terminologies

- Ideally, you have RStudio and administrative rights on your computers. If not,
  use https://rstudio.cloud or 
  https://jupyterhub.wolke.uni-greifswald.de/ (spawn RStudio 4.1).
- Everything is available online at https://gitlab.com/libreumg/teaching/aiir
  - ask for access giving me your gitlab.com login name
- Whenever you have difficulties to follow, please stop me by orally 
  interrupting.
- There are more beginner-oriented slides from the AIIR-track.

# Preface: Some background, definitions, terminologies

- Imperative
- Structure oriented
- Object oriented
- Functional
- Declarative

# Structure oriented

 - Sequence
 - Selection
 - Iteration
 - Abstraction
 
# Scope/Variables/Stack Frames

- Variables
  - names memory places vs. columns in a data table
  
- Stack-Frames/local variables/scopes
  - Digression needed?

# Interests, Needs
  - Do we need more details on programming concepts?
  - How experienced are you in
    - any statistics language (which)
    - any other programming language (which)

# 😇 The Good, R in a nutshell for Computer Scientists -- IDE

  - RStudio
    - The GUI in a nutshell: Demo
    - Tutorials
  - help
    - manual
    - vignettes
```r
? ls
browseVignettes()
```

# 😇 The Good, R in a nutshell for Computer Scientists -- Data

  - Vectors, no scalars -- special scalar, typed value `NA`, `c()`
    - column-wise thinking vs. row-wise thinking in Java, SAS, FORTRAN, SQL,
      ...
  - Copy-On-Write
  - Basic types (element types, cave: character vs. factor)
  - Complex types (list)
  - Environments as generalized closures/stack frames
    - The only relevant reference based type
  - Lazy Evaluation


# 😇 Data

  - Vectors, no scalars -- special scalar, typed value `NA`, `c()`
    - column-wise thinking vs. row-wise thinking in Java, SAS, FORTRAN, SQL,
      ...
![](figures/rowwise.png){height="40px"}
```{js echo=TRUE}
var a1 = {
  "name": "Donald",
  "IQ": 60
}
var a2 = {
  "name": "Albert",
  "IQ": 260
}
var db = [a1, a2];
document.write('<pre class="sourceCode js">')
// print javascript internal representation of the data:
document.write(JSON.stringify(db, null, 2)) // https://stackoverflow.com/a/7220510
document.write('</pre>')
```

# 😇 Data

![](figures/columnwise.png){height="40px"}
```{r echo=TRUE}
tab <- jsonlite::fromJSON('[
  {
    "name": "Donald",
    "IQ": 60
  },
  {
    "name": "Albert",
    "IQ": 260
  }
]')
# print the R internal representation of the data:
str(tab)
```

# 😇 Data - combine data fields

## Many languages think row/record-wise
![](figures/rowwise.png){height="40px"}

```{js echo=TRUE}
var db = [{"name": "Donald", "IQ": 60}, 
          {"name": "Albert","IQ": 260}];
db.forEach(record => document.write(record.name + " has an IQ of " + record.IQ + "<br />"));
```

## R thinks column/field-wise
![](figures/columnwise.png){height="40px"}

```{r echo=TRUE}
tab <- jsonlite::fromJSON('[{"name": "Donald", "IQ": 60}, 
                            {"name": "Albert","IQ": 260}]')

paste(tab$name, "has an IQ of", tab$IQ)
```



# 😇 Data - many versions of `NIL`

```{r echo=TRUE}
l <- list()
l$a <- NULL
l$b <- character(0)
l$c <- c()
l$d <- NA
l$e <- NA_character_
l$f <- NA_complex_
l$g <- as.character(NA)
str(l)
vapply(l, length, FUN.VALUE = integer(1))
```

# 😇 Data

  - Copy-On-Write
  
```{r echo=TRUE, eval=FALSE}
data0 <- cars # no copy, only a reference
data0[1, 1] <- 42 # copies first column of data0
```
 - the amount of copied data depends on the data type

# 😇 Data

  - Basic types (element types), simple operators

```{r echo=TRUE}
identical(1L, (1:4)[1]) # no scalar type
str(1:4) # for debugging
class(1:4)
typeof(1:4); mode(1:4) # more subtle details in the literature, or e.g. https://stackoverflow.com/a/37469255
c(1, 2, "3") # auto-coercion (aka implicit type-casting)
c(1, 2, 3)
c("1", "2", "3")
as.numeric(c("1", "1.2", "4,4")) # explicit coercion never fails, but may generate NAs
f <- as.factor(letters)
c(f)
c(f, 42)
f[[1]] <- "xyz"
print(f)
# operators
1 + 1
2 * 2
3 ** 3
4 / 3
4 %% 3
14 %/% 3
c(TRUE, TRUE) & c(FALSE, TRUE)
FALSE && {print("Never executed"); TRUE}
TRUE && {print("Always executed"); TRUE}
# create a range:
1:10
# multiply
2 * 1:10
# scalar product
1:3 %*% 1:3
```

# 😇 Exercise: RStudio and first steps

- Create a simple R project with an R script that
- prints the numbers from 1 to 10
- prints a multiplication tables for `n*2` and `n*7` with `n` in `0:10`

- Further read: https://www.r-bloggers.com/2022/01/handling-categorical-data-in-r-part-1/; https://stackoverflow.com/a/19410249

# 😇 Data

  - Complex types (list)

```{r echo=TRUE}
a <- list(a = 1:10, x = "test", r = TRUE) # data types differ for different entries
class(a)
attributes(a)
class(a) <- c("test", "asf")
class(a)
attributes(a)
(a)
str(a)
```

# 😇 Data

  - Vectors, no scalars
  - Index vectors

```{r echo=TRUE}
print(letters)
a <- setNames(1:26, letters)
print(a)
print(names(a))
l <- as.list(a[1:4])
print(l)
print(a[[1]]) # access to single elements, returns element
print(a[c(2:4,26)]) # complex index expressions, but slice: returns sub-list/vector
print(l[[1]]) # access to single elements, returns element
print(l[1]) # access to single elements, returns element
print(l[c(2:4,26)]) # complex index expressions, but slice: returns sub-list/vector
print(a[rep(TRUE, 26)]) # index vectors
print(a[c(TRUE, FALSE)]) # automatic repeat
print(a[c(TRUE, FALSE, TRUE)]) # automatic repeat
print(a[c("a", "x")]) # associative arrays / access by name
print(a[["a"]]) # associative arrays / access by name
print(l$b) # for lists, l[["b"]] can be abbreviated
print(a[names(a) %in% c("a", "A", "a", "Alf")]) # complex selection
all(c(TRUE, TRUE, FALSE))
any(c(TRUE, TRUE, FALSE))
a <- array(1:27, # all elements have the same data type
           dim = c(3, 3, 3), 
           dimnames = 
             list(x = letters[1:3], y = letters[24:26], z = sQuote(paste("z", "=", 1:3))))
a # use matrix for the two-dimensional case
```

# 😇 Data

  - Environments as generalized closures/stack frames
    - Similar to HashMaps
    - The only relevant reference based type

```{r echo=TRUE}
e <- new.env(parent = emptyenv())
e$a <- 4
local(e$a <- 20)
e$a
l <- list()
l$a <- 4
local(l$a <- 20)
l$a
print(environment()) # the current environment, usually the global environment
print(environment(rnorm)) # the enclosing environment of the rnorm function
print(parent.env(environment(rnorm))) # hierarchy
print(search()) # environment search path
print(get("print", asNamespace("base"))) # (m)get, assign, ls/names
a <- function() {
  print(environment())
  f <- function(r = 19, ...) {
    print(r)
  }
  f # implicit return of last instruction's result
}
environment(a()) # enclosure of f is the local environment of a
a() # last call implicitly prints result
```


# 😇 Data

  - Lazy Evaluation
  
```{r echo=TRUE}
a <- function() { print("Evaluating a"); 42}
b <- a() + 19 # may be postponed until b is needed (e.g. by force(b))
print("c")
print(b) # at latest, a is called here
```

```{r echo=TRUE}
a <- function() { print("Evaluating a"); 42}
b <- a() + 19 # may be postponed until b is needed (e.g. by force(b))
print("c")
print(b) # at latest, a is called here
```

```{r, echo=TRUE}
delayedAssign("x", a() + 19)
print("not yet")
print(x) # but see package future
```

# 😇 Structured programming

  - Sequence
  - Selection
  - Iteration --> Functional patterns, Copy-On-Write
  - Abstraction

# 😇 Structured programming

  - *Sequence*
  - Selection
  - Iteration --> Functional patterns, Copy-On-Write
  - Abstraction

# 😇 Structured programming

  - *Sequence*

Sequence operator: `;` and line breaks; blocks in braces `{ }`

```{r eval=FALSE, echo=TRUE}
a <- 42; b <- 2
c <- a ^ b
print(c)
```

# Structured programming

  - Sequence
  - *Selection*
  - Iteration --> Functional patterns, Copy-On-Write
  - Abstraction

# Structured programming

  - Sequence
  - *Selection*

Basically `if-then-else`, (`switch` and `ifelse` not really as expected)

```{r eval=FALSE, echo=TRUE}
a <- 42
if (a > 13) print("Test")
if (a > 13) {
  print("Deal") 
} else {
  print("No Deal")
}
```

# 😇 Structured programming

  - Sequence
  - *Selection*

```{r echo=TRUE}
a <- TRUE
b <- T
print(a && b)
cc <- 2 < 1
print(cc)
```
# 😇 Structured programming

  - Sequence
  - *Selection*

Logical operators
```{r echo=TRUE}
a <- print(TRUE)
b <- print(FALSE)
print(paste("a =", a, "b = ", b))
print(b && a)
```

# 😇 Structured programming

  - Sequence
  - *Selection*

Logical operators `&&` and `||` do short-cut
```{r echo=TRUE}
print(FALSE && print(TRUE))
```
```{r echo=TRUE}
print(TRUE && print(FALSE))
```

# Structured programming

  - Sequence
  - *Selection*

Logical operators `&` and `|` do *not* do short-cut
```{r echo=TRUE}
print(FALSE & print(TRUE))
```
```{r echo=TRUE}
print(TRUE & print(FALSE))
```

# 😇 Structured programming

  - Sequence
  - *Selection*

Logical operators `&` and `|` work element-wise, `&&` and `||` use first element
```{r echo=TRUE}
a <- c(TRUE, TRUE, FALSE)
b <- c(FALSE, TRUE, TRUE)
print(a & b)
print(a && b)
a <- c(TRUE, TRUE, FALSE)
b <- c(TRUE, TRUE, TRUE)
print(a && b)
```

# Structured programming

  - Sequence
  - *Selection*
```{r error=TRUE, echo=TRUE}
str(logical(1))
str(logical(0))
if (logical(0)) {}
```

```{r warning=TRUE, echo=TRUE}
if (c(TRUE, FALSE, FALSE)) { print("Deal") }
```

```{r error=TRUE, echo=TRUE}
if (NA) {}
```

# 😇 Structured programming

  - Sequence
  - *Selection*
  
`ifelse` is kind of a ternary operator but vectorized:

```{r}
a <- ifelse(c(TRUE, FALSE, TRUE), 1:3, -1:-3)
a
```

# 😇 Structured programming

  - Sequence
  - *Selection*
The R flavor of `switch` is very data-oriented:
  
```{r echo=TRUE}
gender <- "F"
a <- switch (gender,
  M = "@Home",
  F = "@Work",
  "Depends on Mood"
)
a
```

# 😇 Exercise: 

Given two names memory places, variables `ever_smoker`, and `packyears`, find
contradictions for the scalar case (`n = 1`) and for the vector case (`n > 1`).
Compute the `packyears` for non-smokers as zero:

```{r echo=TRUE}
ever_smoker <- FALSE
packyears <- NA_real_
# compute packyears depending on the values of the two variables
# call warning() for contradictions
print(paste("Result: ", packyears))
```

Repeat for a matrix with two columns:

```{r}
ds0 <- matrix(nrow = 10, ncol = 2)
colnames(ds0) <- c("ever_smoker", "packyears")
ds0[, "ever_smoker"] <- sample(x = c(0, 1), 
                               size = 10, 
                               replace = TRUE)
ds0[, "packyears"] <- sample(x = 0:60, 
                             size = 10, 
                             replace = TRUE)
ds0
ds1 <- ds0
ds1[!ds1[, "ever_smoker"], "packyears"] <- NA
ds1
```


# 😇 Structured programming

  - Sequence
  - Selection
  - *Iteration --> Functional patterns, Copy-On-Write*
  - Abstraction

# 😇 Structured programming

  - Sequence
  - Selection
  - *Iteration --> Functional patterns, Copy-On-Write*

```{r echo=TRUE}
for (i in 1:3) {
  print(i)
}
a <- TRUE
i <- 1
while(a) {
  print(paste("x", i, a))
  i <- i + 1
  a <- i < 3
  print(paste("y", i, a))
}
a <- TRUE
i <- 1
repeat {
  print(paste("x", i, a))
  i <- i + 1
  a <- i < 3
  print(paste("y", i, a))
  if (!a) break;
}
```
# 😇 Structured programming

  - Sequence
  - Selection
  - *Iteration --> Functional patterns, Copy-On-Write*

Compute the sum of the numbers from 1 to `xmax`:
```{r echo=TRUE}
xmax <- 20000
sum(seq_len(xmax))
```

# 😈 The Bad -- Imperative patterns

  - Sequence
  - Selection
  - *Iteration --> Functional patterns, Copy-On-Write*
Not using functional patterns is slow by Copy-On-Write:
```{r echo=TRUE}
x <- integer(0)
for (i in 1:xmax) {
  x <- c(x, i)
}
sum(x)
```
# 😇 Structured programming

  - Sequence
  - Selection
  - *Iteration --> Functional patterns, Copy-On-Write*
Not using functional patterns is not necessarily slow by Copy-On-Write, but 
disrecommended:
```{r echo=TRUE}
xmax <- 20000
x <- integer(xmax)
x[1] <- 0
for (i in 1:xmax) {
  x[i] <- i
}
sum(x)
```

# 😇 Structured programming

  - Sequence
  - Selection
  - Iteration --> Functional patterns, Copy-On-Write
  - *Abstraction*
  
```{r echo=TRUE}
a <- function(x) x * x
a(12)
r <- integer(10)
for (i in seq_len(10)) {
  r[i] <- a(i)
}
r
```

```{r echo=TRUE}
formals(a)
b <- function(a = 42, ..., x) c(...)
formals(b)
```


# 😇 Funcitonal patterns (Python, JavaScript)

  - iterate by apply
  - encapsulation by closures

# 😇 Funcitonal patterns (Python, JavaScript)

  - *iterate by apply*
  - encapsulation by closures

# 😇 Funcitonal patterns (Python, JavaScript)

  - *iterate by apply*
```{r echo=TRUE}
a <- function(x) x * x
a(12)
r <- sapply(seq_len(10), a)
r
```
```{r echo=TRUE}
a <- function(x) { if (x == 3) {integer(0)} else {x * x} }
a(12)
r <- sapply(seq_len(10), a)
r
```

```{r echo=TRUE}
a <- function(x) { x * x }
a(12)
r <- lapply(seq_len(10), a)
r
```


```{r, echo=TRUE}
a <- function(x) { x * x }
a(12)
r <- vapply(seq_len(10), a, FUN.VALUE = integer(1))
r
```

```{r error=TRUE, echo=TRUE}
a <- function(x) { if (x == 3) {integer(0)} else {x * x} }
a(12)
r <- NULL
r <- vapply(seq_len(10), a, FUN.VALUE = integer(1))
r
```

# 😇 Exercise: Elementary school

- write a script that computes all multiplication tables up to 10x10
- use Caesar's cipher to "encrypt" a string stored in `s` given a key 
  integer stored in `k`

# 😇 Funcitonal patterns (Python, JavaScript)

  - iterate by apply
  - *encapsulation by closures*

# 😇 Funcitonal patterns (Python, JavaScript)

  - iterate by apply
  - encapsulation by closures
```{r echo=TRUE}
newPerson <- function(name) {
  .name <- name
  this <- environment()
  list(
    getName = function() {
      .name
    },
    setName = function(n) {
      oldName <- this$.name
      this$.name <- n
      invisible(oldName)
    }
  )
}
Adrian <- newPerson("Dr. Richter")
print(Adrian$getName())
Adrian$setName("Professor Dr. Richter")
print(Adrian$getName())
Adrian$setName("Dr. Richter")
print(Adrian$getName())
```
```{r echo=TRUE}
ls(environment(Adrian$getName), all.names = TRUE)
```

💡 Note: Each lambda comes with its entire enclosure.

# 😇 Exercise: First Larger Project

- Write a program that computes the mean speed of the cars from the
cars data set.
- Append a column to this data set that contains then speed, the dist and the
row-number as a string.
- select each second row from the cars data set.
- Write a small address database class storing addresses as objects.

# Object oriented patterns

  - Generics can act polymorphic on objects of different classes

S3 classes, functional (alternatives are S4, RC, R6 classes)

```{r echo=TRUE}
moron <- list(surname = "Trom", givenname = "Dunal", age = 70)

print(moron)

class(moron)

attr(moron, "row.names") <- "0001"

print(moron)
print.data.frame(moron)

class(moron) <- "data.frame"
class(moron)

print(moron) # is dispatched now to the print generic for data.frames 

# ususally use a constructor names as the class is named:
moron2 <- data.frame(surname = "Trom", givenname = "Dunal", age = 70, row.names = "0001")
print(moron2) # see also tibble for a more modern alternative for data.frame
# similar to matrices, but data types can differ column wise
```


# 😇 Declarative elements

  - Formula and "non-standard-evaluation" enable definition of rules

```{r echo=TRUE}
subset(cars, speed > 4, dist, drop = TRUE)
```


```{r echo=TRUE}
a <- ~ speed * 19
str(a)
a
```
```{r echo=TRUE}
DT::datatable(cars)
lazyeval::f_eval(a, data = cars)
```
  
  - Recursion: not declarative but `Recall`
    - but see packages `drake` and `targets`
  
```{r echo=TRUE}
fib <- Vectorize(function(i) {
  if (i < 1) {
    0
  } else if (i < 2) {
    1
  } else {
    Recall(i - 1) + Recall(i - 2)
  }
})
fib(0:10)
```



<!--
# Appendix, advanced R specific hints

https://stackoverflow.com/a/37469255

```{r}
x <- 1L
print(c(class(x), mode(x), storage.mode(x), typeof(x)))

x <- 1
print(c(class(x), mode(x), storage.mode(x), typeof(x)))

x <- letters
print(c(class(x), mode(x), storage.mode(x), typeof(x)))

x <- TRUE
print(c(class(x), mode(x), storage.mode(x), typeof(x)))

x <- cars
print(c(class(x), mode(x), storage.mode(x), typeof(x)))

x <- cars[1]
print(c(class(x), mode(x), storage.mode(x), typeof(x)))

x <- cars[[1]]
print(c(class(x), mode(x), storage.mode(x), typeof(x)))

x <- matrix(cars)
print(c(class(x), mode(x), storage.mode(x), typeof(x)))

x <- array(1:10, dim = c(2, 5))
print(c(class(x), mode(x), storage.mode(x), typeof(x)))

x <- new.env()
print(c(class(x), mode(x), storage.mode(x), typeof(x)))

x <- expression(1 + 1)
print(c(class(x), mode(x), storage.mode(x), typeof(x)))

x <- quote(y <- 1 + 1)
print(c(class(x), mode(x), storage.mode(x), typeof(x)))

x <- ls
print(c(class(x), mode(x), storage.mode(x), typeof(x)))
```
-->



# 😝 Access to any- and everything is possible

So a very tidy test-driven programming style is recommended.

The Ugly -- Bad Practices:
```{r echo=TRUE}
f1 <- function() {
  a <- 42
  f2()
  return(a)
}
f2 <- function() {
  a <- 19
}
f1()
f2 <- function() {
  e <- parent.frame()
  e$a <- 19
}
f1()
2 * 2
`*` <- function(a, b) a - b
2 * 2
`*` <- base::`*`
2 * 2
unlockBinding("*", asNamespace("base"))
assign("*", function(a, b) {0}, envir = asNamespace("base"))
lockBinding("*", asNamespace("base"))
2 * 2
.Primitive("*")(2, 2)
unlockBinding("*", asNamespace("base"))
assign("*", .Primitive("*"), envir = asNamespace("base"))
lockBinding("*", asNamespace("base"))
2 * 2
```


# generalized exception handling, conditions

Warnings are by default displayed as soon as R displays the command prompt
or exits execution so that they are unlikely being missed.

```{r include=FALSE}
# emulate non-knitr-default-warning behaviour
ws <- list()
orig <- knitr::knit_hooks$get("warning")
knitr::knit_hooks$set(warning = function(x, options) { 
  ws <<- c(ws, x)
  invisible(NULL)
})
```


```{r warning=TRUE, echo=TRUE}
a <- function(i) {
  print(i)
  warning(i)
}
x <- lapply(1:10, a)
str(x)
```

```{r include=FALSE}
# emulate non-knitr-default-warning behaviour
knitr::knit_hooks$set(warning = orig)
```

```{r echo=FALSE, warning=TRUE}
# emulate non-knitr-default-warning behaviour
for (w in seq_along(ws)) {
  warning(ws[[w]])
#  str(ws[[w]])
}
```


# Use `try` or `tryCatchLog::tryLog`:
```{r echo=TRUE}
a <- try(stop("xyz"), silent = TRUE)
str(a)
a <- tryCatchLog::tryLog(stop("xyz"))
str(a)
inherits(a, "try-error")
```

```{r echo=TRUE}
test <- function() {
  stop("An error")
}
tryCatch(test(), error = str)
print(42)
```


```{r echo=TRUE}
test <- function() {
    cond <- simpleCondition("message", call = sys.call())
    signalCondition(cond)
    warning("Test")
}

withCallingHandlers({
    test()
  },
  condition = function(cond) {
    str(cond)
    if (inherits(cond, "simpleWarning")) invokeRestart("muffleWarning")
  }
)
```

```{r echo=TRUE}
a <- tryCatchLog::tryCatchLog({
  stop("xyz")
  print("never executed")
}, error = function(cond) { print("An error is handled"); str(cond) })
str(a)
inherits(a, "try-error")
```
```{r echo=TRUE}
a <- withRestarts(tryCatchLog::tryCatchLog(
  {
    stop("xyz")
    print("never executed")
  }, error = function(cond) { print("An error is handled"); str(cond); invokeRestart("skipxyz") }),
  skipxyz = function(e) {print("Skipping xyz"); structure("", class = "my-try-error")})
str(a)
inherits(a, "my-try-error")
```

# `source()` and Packages

`source`, the poor man's organization of R code in projects

```{r echo=TRUE}
cat(readLines("my_lib.R"))
source("my_lib.R")
greet()
```

# `source()` and Packages

Usually, prefer packages with the following must-haves:

```
DESCRIPTION
R/
```

Support in RStudio. Details on demand.

# Debugging

![Debugging](https://github.com/allisonhorst/stats-illustrations/raw/7212146981eaa6bd681bb7cafd41be0ac705eec4/other-stats-artwork/debugging.jpg){height="500px"}

`browser()` enters debug mode, `debug()` flags a function to be traced in debug mode.
`trace` is a very generic version of debug, but it is meant to be used by IDEs generally.

# Plotting 1

Besides plot, which draws to the selected graphcis device, there are graphical
objects, most prominently created by `ggplot2` (gg = graph-grammar).

```{r echo=TRUE}
plot(x = cars$speed, y = cars$dist)
```

# Plotting 2

```{r echo=TRUE}
library(ggplot2)
p <- ggplot(data = cars, 
            mapping = aes(x = speed, y = dist)) + # aesthetics
  geom_point() # "add" graphs based on the data and mapping above
print(p) # for graphical objects, print the results
```

Details on demand.

# Writing reports: RMarkdown, knitr

Easily create all types of output formats including interactive web sites, 
PowerPoint slides, PDFs. Besides that, `rio` supports reading and writing
data format, such as CSV, Excel Files, CSV, SAS, STATA, etc.

![Why should I learn R?](figures/15-whyR.png){height="600px"}

# Reading data: rio

```{r echo=TRUE}
# install.packages("rio")
library(rio)
demo <- import("ExerciseCoercion.xlsx")
# View(demo)
```

# 😇 Exercise: Excel and data science, a love-hate relationship

- Find out, why the imported Excel file looks so strange

# 😇 usethis for many aspects of package development

```r
usethis::use_description()
usethis::use_roxygen_md()
usethis::use_r("new_dd")
devtools::load_all() # avoids build and reload during development
devtools::check()
usethis::use_test()
my_dd <- new_dd()
```


# 😇 Exercise: We develop a small package -- together

- create a package for maintaining a data dictionary stored in a data frame
- add some functions to help filling the DD from example data
- add some functions for validation
- add some functions for adding/modifying entries manually
- see https://gitlab.com/libreumg/teaching/Demo/-/tree/master/ for start

# Further reading: 1 The Tidyverse

![](https://raw.githubusercontent.com/allisonhorst/stats-illustrations/master/rstats-artwork/tidydata_3.jpg){height="500px"}
- introduce code conventions, most prominently snake_case for all function names
- use strict testing

# Further reading: 1 The Tidyverse

- write anything as pipelines
```{r echo=TRUE}
magrittr::`%>%` # or |> in R >= 4.1.0
cars |> nrow()
```
- more details on demand.

# Further reading: 2 Non-Standard-Evaluation NSE
```{r echo=TRUE}
a <- function(...) { print(substitute(...)) }
a(42 + 19)
```
```{r echo=TRUE}
eval(a(42+19))
```

Details on demand.

# Further reading: 3 RMarkdown, knitr, pandoc

Demo RMarkdown

# Further reading: 4 DBI, `dbx`

Demo DBI, `dbx`, https://www.r-bloggers.com/2022/02/working-with-databases-and-sql-in-rstudio/

# Further reading: 5 Rcpp, Python, Java

Demo on request Rcpp, reticulate, Rserve, rJava

# Further reading: 6 Shiny

Demo on dynamic web apps

# More nifty details

Please check or R developer meeting SHIP-PER 
(https://gitlab.com/umg_hgw/ship-per/ship-per). Ask me for access, if you do not
have yet.

# Variables, Scopes -- digression

```{c echo=TRUE, results='hide'}
#include <stdio.h>
void test() {
  int a = 42;
  Rprintf("%d\n", a);
  a = 49;
  Rprintf("%d\n", a);
}
```
```{r echo=TRUE}
invisible(.C('test'))
```
```{r echo=TRUE}
invisible(.C('test'))
```

# Variables, Scopes -- digression

```{c echo=TRUE, results='hide'}
#include <stdio.h>
int a = 42;
void test2() {
  Rprintf("%d\n", a);
  a = 49;
  Rprintf("%d\n", a);
}
```
```{r echo=TRUE}
invisible(.C('test2'))
invisible(.C('test2'))
```

# Variables, Scopes -- digression

```c
#include <stdio.h>
void test3() {
  int a = 42;
  Rprintf("%d\n", a);
}
void test4() {
  a = 49;
  Rprintf("%d\n", a);
}
```

```c
c120a87bf1e9e1.c:7:3: error: use of undeclared identifier 'a'
  a = 49;
  ^
c120a87bf1e9e1.c:8:18: error: use of undeclared identifier 'a'
  Rprintf("%d\n", a);
                 ^
2 errors generated.
make: *** [c120a87bf1e9e1.o] Error 1
Warning in system(paste("R CMD SHLIB", f), intern = TRUE) :
  running command 'R CMD SHLIB c120a87bf1e9e1.c' had status 1
Error in dyn.load(sub(sprintf("[.]%s$", n), .Platform$dynlib.ext, f)) : 
  unable to load shared object '/Users/struckmanns/git/gitlab/aiir/c120a87bf1e9e1.so':
  dlopen(/Users/struckmanns/git/gitlab/aiir/c120a87bf1e9e1.so, 6): image not found
```

# Variables, Scopes -- digression

```{c echo=TRUE, results='hide'}
#include <stdio.h>
int a = 0;
void test3() {
  int a = 42;
  Rprintf("%d\n", a);
}
void test4() {
  a = 49;
  Rprintf("%d\n", a);
}
```

# Variables, Scopes -- digression

```{r echo=TRUE}
invisible(.C('test3'))
invisible(.C('test4'))
invisible(.C('test3'))
invisible(.C('test4'))
```

# Variables, Scopes -- digression

```{c echo=TRUE, results='hide'}
#include <stdio.h>
void test3() {
  int a = 42;
  Rprintf("%d\n", a);
}
void test4() {
  int a = 49;
  Rprintf("%d\n", a);
}
```

# Variables, Scopes -- digression

```{r echo=TRUE}
invisible(.C('test3'))
invisible(.C('test4'))
invisible(.C('test3'))
invisible(.C('test4'))
```
