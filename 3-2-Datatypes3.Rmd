# Special data types

There are two common but special types of data besides numeric, integer, character, logical, and complex:

- __POSIXct__ (a special format for vectors of dates/datetimes)
- __factors__ (a special format used for categorical data)

Each of these two data types introduces new functionality but may, in the beginning, also cause difficulties.

__*CAVE:*__ Please remember, `R` is very flexible in defining classes of objects. In your work you may encounter further classes, e.g. for time-series measurements the class `ts()`, which then require special care. However, the abovementioned atomic classes plus dates/datetimes and factors will be most frequent.

## Dates and datetime

```{r error=TRUE}
as.POSIXct("2020-02-29")
as.POSIXct("2021-02-29") # careful with Excel files!
x <- as.integer(as.POSIXct("2020-02-29"))
x
as.POSIXct(x)
as.POSIXct(x, origin = as.POSIXct("1970-01-01 01:00.00"))
# besides POSIXct, there are some other date/time types -- default
# should be POSIXct
# To check the internal representation of the different date formats, use
class(as.POSIXct("2020-01-01-23:34:00"))
str(unclass(as.POSIXct("2020-01-01-23:34:00")))
class(as.Date("2020-01-01-23:34:00"))
str(unclass(as.Date("2020-01-01-23:34:00")))
class(as.POSIXlt("2020-01-01-23:34:00"))
str(unclass(as.POSIXlt("2020-01-01-23:34:00")))
```

# Factors

- The concept and use of factors in `R` has pros and cons
- *pro*: in most modeling approaches dummy-coding is not required (for few exceptions please see `fastDummies` package)
- *con*: handling typical use cases of factor labels and levels represents one of the few misconceptions in this language


## Multinomial 

```{r}
# a simple vector, e.g. for sex
x <- c("female", "female", "male", "female", "male")

# class?
class(x)
```

- convert this vector to class factor

```{r}
# check ?factor for the arguments

# make a factor (1)
x_fa <- factor(x, levels = c("female", "male", "diverse"))
x_fa

levels(x_fa)

# applying the function to existing factor
x_fb <- factor(x_fa)
x_fb # drops unused levels!
levels(x_fb)

# applying as.factor() to an existing factor
x_fb <- as.factor(x_fa)
x_fb # does not drop factors
levels(x_fb)
```

## Ordered

```{r}
x <- c("h", "h", "l", "m")
x_o <- factor(x, levels = c("l", "m", "h"))
x_o

x_o <- factor(x, levels = c("l", "m", "h"), ordered = TRUE)
x_o
```

## Common use case and pitfalls with factors

```{r}
x <- c(0, 1, 1, 0, 1)
x

# make numeric vector to multinomial factor
x_f <- factor(x, levels = c(0, 1), labels = c("male", "female"))
x_f


levels(x_f) # upps
```

In this common use case the numerical values of the original vector are not preserved and the recommendation given in the `R help` does not work:

__*To transform a factor f to approximately its original numeric values, as.numeric(levels(f))[f] is recommended and slightly more efficient than as.numeric(as.character(f)).*__

```{r}
as.numeric(levels(x_f))[x_f]

# only works if (remember: x <- c(0, 1, 1, 0, 1)):
x2 <- factor(x) # in this case no labels were assigned to x2
x2 
as.numeric(levels(x2))[x2]

# R uses internal integer representation of factors: 1, 2, 3, ...
as.integer(x_f) 

# get back to numerical values:
ifelse(x_f == "male", 0, 1)

# if more levels other packages may help
x <- sample(0:4, 10, replace = TRUE)
x # numeric
x <- factor(x,
            levels = c(0:4),
            labels = letters[1:5])
x # factor
x <- dplyr::recode(x, 
                   "a" = 0,               
                   "b" = 1,
                   "c" = 2,
                   "d" = 3,
                   "e" = 4) # recoded numeric
x
class(x)
```

## Further problems in handling factors

- combining two factor variables
- dropping levels
- adding levels
- change reference levels
- and many more ...

Please check this [cheat sheet](https://raw.githubusercontent.com/rstudio/cheatsheets/main/factors.pdf).
