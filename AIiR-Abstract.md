---
title: 'AIIR: Applied Introduction Into R'
authors:
  - name: Adrian Richter
    affiliation: 1
  - name: Stephan Struckmann
    affiliation: 1
date: "23 August 2021"
output:
  html_document:
    df_print: paged
  word_document: default
affiliations:
  - name: Institute for Community Medicine, University Medicine Greifswald
    index: 1
---

# Inhalt

R ist eine der beliebtesten und vielseitigsten Software-Distributionen für Data Science. Die Einarbeitung in die Software und insbesondere der Umstieg von einer anderen Software wie STATA oder SAS mag zunächst schwierig erscheinen. Die Mühe lohnt sich jedoch, denn die Verwendung von R ermöglicht nicht nur jegliche statistische Modellierung, sondern auch maschinelles Lernen und reproduzierbare Forschung. Darüber hinaus erleichtert das Wissen einer großen R-Community den Umgang mit der Software und die Lösung individueller wissenschaftlicher Aufgaben.

Für diesen Kurs benötigen die Teilnehmer*Innen keine Voraussetzungen außer einem funktionierenden Computer. Wir beginnen mit der Installation der Software, führen nur die notwendigsten technischen Grundlagen von R ein und lernen die verschiedenen Datentypen sowie grundlegende Datenmanipulationen kennen.

Dieser Kurs bildet die Grundlage für weiterführende Kurse wie "Data-Wrangling-with-R" und "Applied Statistics-with-R".

# Content

R is one among the most popular and versatile software distributions for data science. Getting to know the software and, particularly, switching from another software such as STATA or SAS may appear difficult at first. However, it is worth the efforts as the use of R enables not only for most statistical modeling techniques, but also for machine-learning and reproducible research. In addition, the knowledge provided by a large R community eases the handling of software and to solve individual scientific tasks.

This course has no pre-requisites for students except for a working computer. We will start with the installation of the software, introduce only most necessary technical principles of R, and learn about the different data types and basic data manipulations.

This course builds the basis for intermediate courses such as “Data-Wrangling-with-R” and “Applied Statistics-with-R”.
