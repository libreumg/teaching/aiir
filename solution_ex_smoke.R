compute_smoke <- function(ever_smoker = FALSE, packyears = 0) {
  if (length(ever_smoker) != 1 || length(packyears) != 1) {
    stop("Scalars only, sorry")
  }
  if (!is.logical(ever_smoker) || is.na(ever_smoker)) {
    stop("Ever smoker must be Boolean and not NA")
  }
  if (!is.numeric(packyears)) {
    stop("packyears must be numeric")
  }
  if (!ever_smoker) {
    if (!is.na(packyears) && packyears > 0) {
      warning("A non-smoker has packyears > 0")
    }
  }
}


ever_smoker <- FALSE
packyears <- NA_real_

if (!ever_smoker) {
  if (packyears > 0) {
    stop("Widerspruch!!")
  }
}

if (is.na(packyears)) {
  warning("smokepack-value not known")
}
ever_smoker <- TRUE

if (ever_smoker) {
  if (packyears <= 0) {
    warning("smoker smoked nothing")
  }
} else {
  
}






########## example data for case n > 1 ##################
ds0 <- matrix(nrow = 10, ncol = 2)
colnames(ds0) <- c("ever_smoker", "packyears")
# runif(10, 1, 6)
ds0[, "ever_smoker"] <- sample(x = c(0, 1), 
                               size = 10, 
                               replace = TRUE)
ds0[, "packyears"] <- sample(x = 0:60, 
                             size = 10, 
                             replace = TRUE)
ds0
ds1 <- ds0
ds1[!ds1[, "ever_smoker"], "packyears"] <- NA
ds1

ds2a <- ds1
ds2a[(!ds1[, "ever_smoker"]) & is.na(ds2a[, "packyears"]), "packyears"] <- 0
ds2a

ds2 <- ds0
which((!ds2[, "ever_smoker"]) & (ds2[, "packyears"] > 0))
