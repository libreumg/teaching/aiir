# Setup for the course, Home Directory, Course Working Directory 1

- Open RStudio and find the Console tab:

![](./figures/02-rstudio-console.png "RStudio Console"){width="65%"}

- The file browser in the lower right corner is left to self-instruction

# Setup for the course, Home Directory, Course Working Directory 2

As soon as you start RStudio, a working directory will be used. To check which one is currently set, please type:

```{r eval = FALSE}
# get name of current working directory:
getwd()
```

into the console and press enter.

## Basic

The default working directory is most frequently not ideal. You can/should specify a working directory which suits your project requirements.

1) create such a directory manually
2) copy the path to the directory (e.g. from context menu of Windows Explorer)
3) paste this path into the R function: `setwd()` in the RStudio console (or into your R script)

```{r eval = FALSE}
# set the name of the desired working directory:
setwd("your path with backslash replaced by forward slash")
```

4) and replace all backslash ("\\") by forward slash ("/")

*Note: the backslash in R represents a so-called escape parameter which is used to indicate special characters. Therefore, in R path delimiters have to be specified using a forward slash or double bakslash.*

## Advanced

- Enter the following command in the console:

```{r echo=TRUE, eval=FALSE}
aiir_home <- file.path(path.expand("~"), "aiir")
if (!dir.exists(aiir_home)) {
  dir.create(aiir_home, recursive = TRUE)
}
cat("Directory for course material:", aiir_home)
```
  - This will create a directory for course material we will refer to in
later sessions. It will also print the location to the console.

# Working directory, Projects

-   RStudio runs and controls an R session.
-   Each R session has a working directory
-   All file names without or with
    [*relative*](https://en.wikipedia.org/wiki/Path_(computing)#Absolute_and_relative_paths){target="_blank"}
    path information will refer to this working directory.
    - $\rightarrow$ accessing files in higher/lower file system level requires respective paths
-   RStudio also has the concept of projects -- we will postpone this for now
-   Set your working directory to your newly created course directory with the `setwd` command:
```{r echo=TRUE, eval=FALSE}
setwd(aiir_home)
```

# Help, technical reference

There is a help browser in RStudio (and in R):

![](./figures/03-rstudio-help.png "RStudio Help"){width="70%"}

To get help for an R command, you can use the question-mark command `?`
or the search box of the help browser. Try in your console:

```{r echo=TRUE, eval=FALSE}
? setwd
```

This will open the help browser and display the technical reference for
the `setwd` function.

# Working with R-Files

 - R code is stored in files with the extension `R`. RStudio has an editor for R
   code:
![](./figures/04-rstudio-editor.png "RStudio Editor"){width="70%"}
 - You can open and save files using the File menu
 - You can directly copy and run selected code in the console using the `Run`
   button in the upper right corner of the editor
   - __Recommended__: there is a keyboard-short-cut (usually `Ctrl`-Enter or `Cmd`-Enter) for executing
     code
   - If you click the `Run` button without selecting text, the command at the
     cursor position in the editor is executed

# Course documentation

Either

- Creating an R file for playing around
- Creating a Notebook for playing around and keep notes

