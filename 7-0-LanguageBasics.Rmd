# Language basics

## Selection

  - `if-then-else`
```{r}
a <- 1
b <- 2
if (a == b) {
  42
} else {
  19
}
```


```{r error=TRUE}
a <- NA
if (a == b) {
  42
} else {
  19
}
```

  - `switch`

```{r}
x <- 3
switch (x,
  "1" = "I",
  "2" = "II",
  "3" = "III",
  "4" = "IV",
  "5" = "V",
  "6" = "VI",
  "7" = "VII",
  "8" = "VIII",
  "9" = "IX",
  "10" = "X", 
  "???"
)
```
I prefer named vectors.

 - `ifelse`
 
```{r}
ifelse(1:10 < 3, "k", "ge")
```
 

## Iteration

  - Loops
  - Apply

```{r}
sin(1:10)

for (i in 1:10) {
  print(sin(i * i))
}
```

for apply, we need functions

## Abstraction / Functions

Examples:

  - `sin(pi)`

to write a function, use the `function` keyword:

```{r}

f <- function(x) {
  return(x * x)
}

plot(1:10, f(1:10))

```

```{r}
sin(1:10)

for (i in 1:10) {
  print(sin(i * i))
}

results <- numeric(10)
for (i in 1:10) {
  results[[i]] <- sin(i * i)
}
results


results <- sapply(1:10, function(i) {
  print(sin(i*i))
})
results
```

## Apply and related -- a family of functions

  - `sapply` -- meant for interactive use
  - `lapply` -- most general apply
  - `mapply` -- apply a function to a set of vectors
  - `Map`    -- like  `mapply` but w/o simplify
  - `apply`  -- for 2-dimensional objects like data.frames and arrays
  - `eapply`, ... -- for environments, there may be more
  - `Vectorize` -- decorator making functions that take only scalar arguments
                   work with vector input
