# Exercise: Subsetting

  - Use the `iris` dataset

Try to

  - split it up in 3 dataframes, one for each species (hint: ? split)
    
  - in each sub dataframe and in `iris`, count the number of 
    instances with a Setal Width between 2.0 and 3.0
    
```{r echo=FALSE}
# lapply(lapply(c(split(iris, iris$Species), iris = list(iris)), subset, Sepal.Width >= 2.0 & Sepal.Width <= 3.0), nrow)
```
